from libfcns import makeTransaction, hashMe, json, isValidTxn, updateState, makeBlock, checkChain

# INITIAL BLOCK OF COURSES
studentGrades = [{'Mark':85, 'Sally':95,'John':90,'Donald':75,'Alice':98,'Jean':92}]  # Define the initial state
courseBlockContents = {'institutionName':'Clark College','courseName':'Programming Tools','courseNumber':'CSE224','instructorName':'Nick','dateCompleted':'10DEC2017','durationDays':112,'maxPoints':'100','studentGrades':studentGrades}
genesisBlockTxns = [courseBlockContents]
genesisBlockContents = {'blockNumber':0,'parentHash':None,'txnCount':1,'txns':genesisBlockTxns}
genesisBlock = {'hash':hashMe(genesisBlockContents),'contents':genesisBlockContents}
genesisBlockStr = json.dumps(genesisBlock, sort_keys=True)
chain = [genesisBlock]


# APPEND MULTIPLE CLASSES FROM DIFFERENT INSTITUTION
studentGrades1 = [{'Michael':85, 'Sammuel':95,'Jennifer':90,'Dennis':75,'Alicia':98,'Johnson':92}]  # Define the initial state
courseBlockContents1 = {'institutionName':'Washington State University','courseName':'Advanced Programming Tools','courseNumber':'CSE228','instructorName':'Nicholas','dateCompleted':'15MAY2017','durationDays':112,'maxPoints':'100','txns':studentGrades1}
studentGrades2 = [{'Jordon':85, 'Scotty':95,'Janice':90,'Brasco':75,'Alan':98,'Brian':92}]  # Define the initial state
courseBlockContents2 = {'institutionName':'Washington State University','courseName':'Programming Tools III','courseNumber':'CSE322','instructorName':'JJ','dateCompleted':'15MAY2017','durationDays':112,'maxPoints':'100','txns':studentGrades2}
txnBuffer = [courseBlockContents1, courseBlockContents2]

blockSizeLimit = 5  # Arbitrary number of transactions per block- 
               #  this is chosen by the block miner, and can vary between blocks!

while len(txnBuffer) > 0:
    bufferStartSize = len(txnBuffer)
    
    ## Gather a set of valid transactions for inclusion
    txnList = []
    while (len(txnBuffer) > 0) & (len(txnList) < blockSizeLimit):
        newTxn = txnBuffer.pop()
        txnList.append(newTxn)

    ## Make a block
    myBlock = makeBlock(txnList,chain)
    chain.append(myBlock)

print(chain)
print("")

'''
import copy
nodeBchain = copy.copy(chain)
nodeBtxns  = [makeTransaction() for i in range(5)]
newBlock   = makeBlock(nodeBtxns,nodeBchain)
print(newBlock)
print("")

print("Blockchain on Node A is currently %s blocks long"%len(chain))

try:
    print("New Block Received; checking validity...")
    state = checkBlockValidity(newBlock,chain[-1],state) # Update the state- this will throw an error if the block is invalid!
    chain.append(newBlock)
except:
    print("Invalid block; ignoring and waiting for the next block...")

print("Blockchain on Node A is now %s blocks long"%len(chain))
'''